using System.ComponentModel.Design.Serialization;
using System.Runtime.CompilerServices;
using System.Security.Permissions;
using System.Runtime.InteropServices;
using System.IO.Enumeration;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;
using System.Security.AccessControl;
using System.Threading;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http; //HttpRequest Class
//IFormFile.ContentDisposition
using System.IO;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using DataManager.Models;

namespace DataManager.Controllers
{
    
    //[EnableCors("AllowOrigin")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController:ControllerBase
    {
        private AppIdentityDbContext context;
        private UserManager<User> userManager; 
        
        public FilesController(AppIdentityDbContext context,UserManager<User> userManager) {
           this.context=context;
           this.userManager=userManager;
        }

    [HttpPost, DisableRequestSizeLimit]
    [Route("upload")]
    public IActionResult Upload()
    {
    try
    {   
        FileItem fileItem;
        //var userName =  User.FindFirstValue(ClaimTypes.Name);
        var userName = User.Identity.Name;
        Console.WriteLine("uuu"+User.Identity.Name);
        Console.WriteLine("uu2"+userName);
        var user = userManager.FindByNameAsync(userName).Result;
        Console.WriteLine("uu4"+user.Id);
        var file = Request.Form.Files[0];
        var folderName = Path.Combine("wwwroot", "Data");
        var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);
        if (file.Length > 0)
        {
            //var fileName=file.Name.Trim('"');
            var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            var fullPath = Path.Combine(pathToSave, fileName);
            string dbPath=null; 
            //creating file entity
            if (!System.IO.File.Exists(fullPath)) {
                fileItem=new FileItem() {Name=fileName, NewName=fileName,
                Path=fullPath, User=user};
                dbPath=Path.Combine(folderName, fileName);
            }
            else {
                string newName=getNewName(fileName,pathToSave);
                fullPath = Path.Combine(pathToSave, newName);
                fileItem=new FileItem() {Name=fileName, NewName=newName,
                Path=fullPath, User=user};
                dbPath=Path.Combine(folderName, newName);
            }
            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(stream);
            }
             context.Files.Add(fileItem);
             try{
                 context.SaveChanges();
             }
             catch (Exception ex) {
                 return StatusCode(500,"Cannot save to db");
             }

            return Ok(new { dbPath });
        }
        else
        {
            return BadRequest();
        }
    }
        catch (Exception ex)
        {
        return StatusCode(500, $"Internal server error: {ex}");
        }
    }

    public string getNewName(string fileName, string folder) {
        int i=1;
        string ext=Path.GetExtension(fileName);
        string shortName=Path.GetFileNameWithoutExtension(fileName);
        string newShortName;
        string fullPath=Path.Combine(folder, fileName);
        while(System.IO.File.Exists(fullPath))
        {
        newShortName=shortName+i;
        shortName=newShortName;
        fileName=shortName+ext;
        Console.Write("nnn"+fileName);
        fullPath=Path.Combine(folder, fileName);
        i++;
        }
        return fileName;
    }

    [HttpGet, DisableRequestSizeLimit]
    [Route("getFiles")]
    public IActionResult GetFilesNames()
    {
    try
    {
        string userName=User.Identity.Name;
        var user = userManager.FindByNameAsync(userName).Result;
        Console.WriteLine("uuu6"+context.Files.ToList().Count);
        //Console.WriteLine("uuu5"+context.Entry(user).Reference(u=>u.Files).Load());
        var paths=context.Files.ToList().Where(f=>f.User!=null && f.User.Id==user.Id).Select(f=>f.Path).Where(p=>System.IO.File.Exists(p)).ToList();
        Console.WriteLine("ccc"+paths.Count);
        var folderName = Path.Combine("wwwroot", "Data");
        //var pathToRead = Path.Combine(Directory.GetCurrentDirectory(), folderName);
        //var files = Directory.EnumerateFiles(pathToRead)
        //    .Select(fullPath => Path.Combine(folderName, Path.GetFileName(fullPath)));
        var files=paths.Select(fullPath => Path.Combine(folderName, Path.GetFileName(fullPath)));
        return Ok(new { files });
    }
      catch (Exception ex)
      {
        return StatusCode(500, $"Internal server error: {ex}");
      }
    }

    [HttpGet, DisableRequestSizeLimit]
    [Route("download")] //[FromQuery]
    public IActionResult Download(string fileUrl)
    {
    Console.WriteLine("qqq "+fileUrl);
    var filePath = Path.Combine(Directory.GetCurrentDirectory(), fileUrl);
    Console.WriteLine(filePath);
    if (!System.IO.File.Exists(filePath))
        return NotFound();
    var memory = new MemoryStream();
    using (var stream = new FileStream(filePath, FileMode.Open))
    {
        stream.CopyTo(memory);
    }
        Console.WriteLine("LL1"+memory.Length);
        memory.Position = 0;
        var f=File(memory, GetContentType(filePath), filePath);
        Console.WriteLine("LL2"+f.FileStream.Length);
        return f;
    }
    private string GetContentType(string path)
    {
    var provider = new FileExtensionContentTypeProvider();
    string contentType;
            
    if (!provider.TryGetContentType(path, out contentType))
    {
        contentType = "application/octet-stream";
    }     
    return contentType;
    }


    }
}