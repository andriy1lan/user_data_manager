using System.Transactions;
using System.Security.AccessControl;
using System.Threading.Tasks;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Diagnostics.Tracing;
using System.Diagnostics;
using System.Reflection.Metadata;
using System;
using System.Linq;
using System.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Cors;
using DataManager.Models;

namespace DataManager.Controllers
{
    //[EnableCors("AllowOrigin")]  
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController: ControllerBase
    {
        private readonly UserManager<User> userManager;  
        private readonly RoleManager<Role> roleManager;
        private readonly SignInManager<User> signInManager;
        private readonly IConfiguration config;

         public UsersController(UserManager<User> userManager,RoleManager<Role> roleManager,
         SignInManager<User> signInManager,IConfiguration config) {
             this.userManager=userManager;
             this.roleManager=roleManager;
             this.signInManager=signInManager;
             this.config=config;
         }

         // GET api/users
        [HttpGet]
        public ActionResult<IList<UserDTO>> GetUsers()
        {
          UserDTO userDTO;
          IList<UserDTO> usersDTO=new List<UserDTO>();
          var users = userManager.Users.ToListAsync().Result;
            if (users.Count!= 0)
            {
                foreach(var user in users) {
                    string roleName=userManager.GetRolesAsync(user).Result[0];
                    userDTO=UserMapper.mapUser(user,roleName);
                    usersDTO.Add(userDTO);
                }
                return Ok(usersDTO);
            }
            else { return NotFound(); }

        }

        // GET api/users/5
        [HttpGet("{id}")]
        public ActionResult<UserDTO> GetUser(int id)
        {   
            UserDTO userDTO;
            var user=userManager.FindByIdAsync(id.ToString()).Result; //id.ToString();
            //var user = userManager.Users.FirstOrDefault(u => u.Id == id);
            if (user!= null)
            {
                Console.WriteLine("ro00 "+user.Id);
                string roleName=userManager.GetRolesAsync(user).Result[0];
                userDTO=UserMapper.mapUser(user,roleName);
                return Ok(userDTO);
            }
            else { return NotFound(); }
        }

        // GET api/users/5
        [HttpGet("name/{username}")]
        public ActionResult<UserDTO> GetUser(string username)
        {   
            UserDTO userDTO;
            var user=userManager.FindByNameAsync(username).Result;
            if (user!= null)
            {
                string roleName=userManager.GetRolesAsync(user).Result[0];
                userDTO=UserMapper.mapUser(user,roleName);
                return Ok(userDTO);
            }
            else { return NotFound(); }
        }
        
        /* 
        public string getRoleName(User user) {
              string roleName=null;
              Console.WriteLine("ro1 "+user.UserRoles);
              Console.WriteLine("ro12 "+user);
              if(user.UserRoles!=null) {
              if (user.UserRoles.Count!=0) {
              roleName=roleManager.FindByIdAsync(user.UserRoles.ToList()[0].Role.Id.ToString()).Result.Name;
                           }             
              }
              Console.WriteLine("ro2 "+roleName);
              return roleName;
        } */

        // POST api/users/auth
        [HttpPost]
        [Route("auth")]
        public ActionResult<User> LoginUser([FromBody]Login login)
        {
            if (!ModelState.IsValid) return BadRequest("No login or password");
            User user = userManager.FindByNameAsync(login.UserName).Result;
                if (user != null)
                {
                    var passwordCheck = signInManager.CheckPasswordSignInAsync(user, login.Password, false).Result;
                    if (passwordCheck.Succeeded) {
                        var claims = new List<Claim>
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.UserName), //user.Id
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                            new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName)
                        };
                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.config["Tokens:Key"]));
                        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
                        var tokenDescriptor = new SecurityTokenDescriptor
                        {
                            Subject = new ClaimsIdentity(claims),
                            Expires = DateTime.UtcNow.AddHours(2),
                            SigningCredentials = credentials
                        };
                        var tokenHandler = new JwtSecurityTokenHandler();
                        var token = tokenHandler.CreateToken(tokenDescriptor);
                        return Ok(new
                        {
                            token = tokenHandler.WriteToken(token),
                            username=user.UserName,
                            authority=userManager.GetRolesAsync(user).Result[0]
                        });
                    }
                }
            return Unauthorized();
        }
 
        // POST api/users/register
        [HttpPost]
        [Route("register")]
        public ActionResult<string> PostUser([FromBody]RegisterDTO registerDTO)
        {
           var userExists = userManager.FindByNameAsync(registerDTO.UserName).Result;  
            if (userExists!= null)  
                return StatusCode(409,"User already exists!");

         User user=new User() {
             Name=registerDTO.Name, UserName=registerDTO.UserName,
             Email=registerDTO.Email
         };
         var result=userManager.CreateAsync(user,registerDTO.Password).Result;
         if (!result.Succeeded) {
         return StatusCode(500, "User did not registered. Check details");}

            string roleName=registerDTO.isAdmin?"Admin":"User";
            Console.WriteLine("rr1 "+roleName);
            var role=roleManager.FindByNameAsync(roleName).Result;
            Console.WriteLine("rr2 "+role);
            if (role!=null)
                {
                    Console.WriteLine("rr22 "+role.Name);
                    userManager.AddToRoleAsync(user,role.Name).Wait();
                }
            return Ok();
         }

         // PUT api/users/5
        [HttpPut("{id}")]
        public ActionResult<User> PutUser(int? id, [FromBody] UserDTO userDTO)
        {
            string roleName=userDTO.isAdmin?"Admin":"User";
            User user=null;
            if (id!=null && id!=0)  
            {  
                user = userManager.FindByIdAsync(id.ToString()).Result;  
                if (user != null)  
                {  
                    user.Name = userDTO.Name;  
                    user.UserName = userDTO.UserName;
                    user.Email = userDTO.Email;    
                }
                else return BadRequest("No such user"); 
            }
            else return BadRequest("No correct id defined"); 
            string existingRoleName = userManager.GetRolesAsync(user).Result[0]; 
            IdentityResult result = userManager.UpdateAsync(user).Result;  
                    if (result.Succeeded)  
                    {   
                        if (existingRoleName!= roleName)  
                        {   
                            IdentityResult roleResult = userManager.RemoveFromRoleAsync(user, existingRoleName).Result;  
                            if (roleResult.Succeeded)  
                            {     
                                  Role newRole = roleManager.FindByNameAsync(roleName).Result;  
                                  if (newRole != null)  
                                  {   
                                      IdentityResult newRoleResult = userManager.AddToRoleAsync(user, newRole.Name).Result;  
                                        if (newRoleResult.Succeeded)  
                                        {  
                                        if (!String.IsNullOrEmpty(userDTO.Password)) {
                                            userManager.RemovePasswordAsync(user).Wait();
                                            userManager.AddPasswordAsync(user,userDTO.Password).Wait(); }
                                        return Ok(); 
                                        }
                                   }      
                            
                            } 
                            else {
                            return StatusCode(500, 
                            "User did not updated. Check details");
                            }
                        } 
                        else {
                           if (!String.IsNullOrEmpty(userDTO.Password)) {
                                            userManager.RemovePasswordAsync(user).Wait();
                                            userManager.AddPasswordAsync(user,userDTO.Password).Wait(); }
                            return Ok();
                        }  
                    }   
            return StatusCode(500, "User did not updated. Check details");      
        }
        
        // DELETE api/users/5
        [HttpDelete("{id}")]
        public ActionResult<User> DeleteUser(int? id)
        {
            if (id!=null && id!=0)  
            {
                User userD = userManager.FindByIdAsync(id.ToString()).Result;  
                if (userD!= null)  
                {  
                    IdentityResult result = userManager.DeleteAsync(userD).Result;   
                    if (result.Succeeded)  
                    {  
                        return Ok("User Deleted");  
                    } 
                    else return StatusCode(500, "User did not deleted"); 
                }
                else return StatusCode(500, "User did not deleted"); 
            }
            else return BadRequest("No such user"); 
        }

    }
}