using System.Data.Common;
using System.ComponentModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using DataManager.Models;
namespace DataManager.Controllers
{
    //[EnableCors("AllowOrigin")]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController:ControllerBase
    {
       private AppIdentityDbContext context;
       public GroupsController(AppIdentityDbContext context) {
           this.context=context;
       }
        
        // GET api/groups
        [HttpGet]
        public ActionResult<IList<GroupDTO>> GetGroups()
        {
            var groups = context.Groups.ToList();
            if (groups.Count!= 0)
            {
                IList<GroupDTO> groupsDTO=new List<GroupDTO>();
                foreach(var group in groups) {
                   var groupWithUsers = context.Groups.Include(gr => gr.UserGroups).ThenInclude(row => row.User).FirstOrDefault(gr => gr.GroupID == group.GroupID);
                   IList<User> Users2 = groupWithUsers.UserGroups.Select(row => row.User).ToList();
                   GroupDTO groupDTO=new GroupDTO () {
                   GroupID=group.GroupID, Name=group.Name,
                   Users=UserMapper.mapListForGroup(Users2) 
                   };
                   groupsDTO.Add(groupDTO);
                }
                return Ok(groupsDTO);
            }
            else { return NotFound(); }
        }

         // GET api/groups/5
        [HttpGet("{id}")]
        public ActionResult<GroupDTO> GetGroup(int id)
        {
            var group = context.Groups.Where(g=>g.GroupID.Equals(id)).FirstOrDefault();
            if (group==null) return NotFound();
            var groupWithUsers = context.Groups.Include(gr => gr.UserGroups).ThenInclude(row => row.User).FirstOrDefault(gr => gr.GroupID == id);
            IList<User> Users2 = groupWithUsers.UserGroups.Select(row => row.User).ToList();
            if (group != null)
            {
                GroupDTO groupDTO=new GroupDTO () {
                GroupID=group.GroupID, Name=group.Name,
                Users=UserMapper.mapListForGroup(Users2) 
                };
                return Ok(groupDTO);
            }
            else { return NotFound(); }
        }
        
        // POST api/groups
        [HttpPost]
        public ActionResult<Group> PostGroup([FromBody]GroupDTO groupDTO)
        {
             Group group=context.Groups.Where(g=>g.Name.Equals(groupDTO.Name)).FirstOrDefault();
             if (group!=null) return BadRequest("Such groupname already exists");
             group=new Group {
                 Name=groupDTO.Name
             };
             context.Groups.Add(group);
             context.SaveChanges();
             int groupId=group.GroupID;
             group.UserGroups=new Collection<UserGroup>();
             foreach(UserDTO u in groupDTO.Users) {
                UserGroup ug=new UserGroup{
                    UserId=u.Id, GroupId=groupId
                };
             group.UserGroups.Add(ug);
             }
             try{
                 context.SaveChanges();
             }
             catch (Exception ex) {
                 return StatusCode(500,"Cannot save new userGroup");
             }
            return StatusCode(201);
        }

        private IList<UserDTO> GetNewUsers(IList<UserDTO> oldUsers, 
        IList<UserDTO> newUsers) {
            IList<UserDTO> users=new List<UserDTO>();
            foreach(var ud in newUsers) {
                if (!oldUsers.Any(u=>u.Id==ud.Id))
                users.Add(ud);
            }
            return users;
        } 


        [HttpPut("{id}")]
        public ActionResult<Group> PutGroup(int? id, [FromBody] GroupDTO groupDTO)
        {
            if (groupDTO.GroupID!=id) return BadRequest();
            Group group=context.Groups.Find(id);
            if (group==null) return NotFound("No group to update");
            if (group.Name.Equals(groupDTO.Name) && groupDTO.Users==null)
            return BadRequest("No new data to update");
            group.Name=groupDTO.Name;
            var groupWithUsers = context.Groups.Include(gr => gr.UserGroups).ThenInclude(row => row.User).FirstOrDefault(gr => gr.GroupID == id);
            IList<User> Users2 = groupWithUsers.UserGroups.Select(row => row.User).ToList();
            Console.Write(Users2.Select(u=>u.Id));
            IList<UserDTO> oldUsers=UserMapper.mapListForGroup(Users2);
            if (!groupDTO.usersToRemove) {
                foreach(UserDTO u in groupDTO.Users) {
                UserGroup ug=new UserGroup{
                    UserId=u.Id, GroupId=(int)id
                };
             group.UserGroups.Add(ug);
               }
            }
            else {
            ICollection<UserGroup> ugs=new Collection<UserGroup>();
            foreach(UserDTO u in groupDTO.Users) {
                /* oldUsers.Any(o=>o.Id==u.Id)
                UserGroup ug0=new UserGroup{
                    UserId=u.Id, GroupId=(int)id
                }; */
                group.UserGroups.Remove(group.UserGroups.First(ug=>ug.UserId==u.Id && ug.GroupId==id));
              }
            }
            try{
                    context.SaveChanges();
                    }
                    catch (Exception ex) {
                        return StatusCode(500,"Cannot update user Group");
                    }
                    return Ok();
           }

        // DELETE api/groups/5
        [HttpDelete("{id}")]
        public ActionResult<Group> DeleteGroup(int? id)
        {
               Group group=context.Groups.Find(id);
               if (group==null) return NotFound("No such group to delete");
               else {
                    var groupWithUsers = context.Groups.Include(gr => gr.UserGroups).First(gr => gr.GroupID == id);
                    context.RemoveRange(groupWithUsers.UserGroups);
                    context.Groups.Remove(group);
                    try{
                    context.SaveChanges();
                    }
                    catch (Exception ex) {
                        return StatusCode(500,"Cannot remove user Group");
                    }
                    return Ok("Group Deleted"); 
               }
        }

        



        

        





       



    }
}