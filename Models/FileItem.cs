using System.ComponentModel.DataAnnotations;
namespace DataManager.Models
{
    public class FileItem
    {
        [Key]
        public int Id {get;set;}
        public string Name {get;set;}
        public string NewName {get;set;}
        public string Path {get;set;}
        public User User {get;set;}
    }
}