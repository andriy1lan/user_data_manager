using System.ComponentModel.DataAnnotations;

namespace DataManager.Models
{
    public class UserDTO
    {
        [Required(ErrorMessage = "Id is required")]  
        public int Id { get; set; } 

        public string Name { get; set; } 
        
        [Required(ErrorMessage = "UserName is required")]  
        public string UserName { get; set; }  
  
        public string Email { get; set; }

        public string Password { get; set; }

        public bool isAdmin {get; set;}
    }
}