using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace DataManager.Models
{
    public class User: IdentityUser<int>
    {
        public string Name {get; set;}
        public virtual ICollection<UserRole> UserRoles  {get; set;}
        public virtual ICollection<UserGroup> UserGroups  {get; set;}
        public virtual ICollection<FileItem> Files  {get; set;}
    }
}