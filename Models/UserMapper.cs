using System.Threading.Tasks;
using System.Reflection.Metadata;
using System.Collections.Generic;
namespace DataManager.Models
{
    public class UserMapper
    {
        public static UserDTO mapUser(User user, string roleName) {
          UserDTO userDTO=new UserDTO();
          userDTO.Id=user.Id; userDTO.Name=user.Name;
          userDTO.UserName=user.UserName; userDTO.Email=user.Email;
          if(roleName!=null) {
          userDTO.isAdmin=roleName.Equals("Admin")?true:false; }
          return userDTO;
        }

        public static UserDTO mapForGroup(User user) {
          UserDTO userDTO=new UserDTO();
          userDTO.Id=user.Id; userDTO.UserName=user.UserName;
          return userDTO;
        }

        public static IList<UserDTO> mapListForGroup(IList<User> users) {
          //if (users==null) return null;
          IList<UserDTO> usersDTO=new List<UserDTO>();
          foreach (var user in users) {
          UserDTO userDTO=new UserDTO();
          userDTO.Id=user.Id; userDTO.UserName=user.UserName;
          usersDTO.Add(userDTO);
          }
          return usersDTO;
        }
    }
}