using System.Collections.Generic;

namespace DataManager.Models
{
    public class GroupDTO
    {
        public int GroupID {get;set;}
        public string Name {get;set;}
        public IList<UserDTO> Users {get;set;}
        public bool usersToRemove {get;set;}
        //true - remove the users from the group, false - add to the group
    }
}