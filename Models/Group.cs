using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataManager.Models
{
    public class Group
    {
        [Key]
        public int GroupID {get;set;}
        public string Name {get;set;}
        public virtual ICollection<UserGroup> UserGroups  {get; set;}
    }
}