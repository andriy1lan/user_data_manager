using System.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using DataManager.Models;

namespace DataManager.Models
{
    
    public class AppIdentityDbContext:IdentityDbContext<User,Role,int>
    {
        public DbSet<Group> Groups {get;set;}
        public DbSet<FileItem> Files {get;set;}
        
        public AppIdentityDbContext(DbContextOptions<AppIdentityDbContext> options)  
         : base(options) {
         }

          protected override void OnModelCreating(ModelBuilder modelBuilder)
          {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>(b =>
            {
            // Each User can have many entries in the UserRole join table
            b.HasMany(e => e.UserRoles)
                .WithOne(e => e.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();
            });
            modelBuilder.Entity<Role>(b =>
            {
            // Each Role can have many entries in the UserRole join table
            b.HasMany(e => e.UserRoles)
                .WithOne(e => e.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();
            });

            modelBuilder.Entity<UserGroup>()
            .HasKey(ug => new { ug.UserId, ug.GroupId });  
            modelBuilder.Entity<UserGroup>()
            .HasOne(ug => ug.User)
            .WithMany(u => u.UserGroups)
            .HasForeignKey(ug => ug.UserId);  
            modelBuilder.Entity<UserGroup>()
            .HasOne(ug => ug.Group)
            .WithMany(g => g.UserGroups)
            .HasForeignKey(ug => ug.GroupId);
            //preseed roles table with Admin and User roles
            modelBuilder.Entity<Role>().HasData(new Role {Id =1, Name = "Admin", NormalizedName = "ADMIN"});
            modelBuilder.Entity<Role>().HasData(new Role {Id =2, Name = "User", NormalizedName = "USER"});

          }  

    }
}

